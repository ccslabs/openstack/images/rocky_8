source "openstack" "rocky" {
    flavor       = "m1.small"
    image_name   = "rockly_8_ccslabs"
    networks     = ["a8bf8f65-5ee3-42db-998f-5d32e78a5a0b"]
    source_image = "6b94413d-37fe-42a3-9a3e-2256ab39af32"
#    external_source_image_url = "https://download.rockylinux.org/pub/rocky/8/images/x86_64/Rocky-8-GenericCloud-Base.latest.x86_64.qcow2"
#    user_data_file = "./cloud-init.yml"
    ssh_username = "rocky"
    volume_type  = "rbd"
    volume_size  = "5"
}

build {
  sources = ["source.openstack.rocky"]

  provisioner "ansible" {
    playbook_file   = "./ansible/site.yml"
    use_sftp        = false
    user            = "rocky"
  }
}